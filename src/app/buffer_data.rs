use std::ptr;
use ash::vk;

use crate::utils::buffer_utils;

pub struct BufferData {
    pub buffer: vk::Buffer,
    pub mem: vk::DeviceMemory,
    pub size: vk::DeviceSize,
}

impl BufferData {
    #[allow(clippy::too_many_arguments)]
    pub fn new(instance: &ash::Instance, device: &ash::Device, phys_device: vk::PhysicalDevice,
                     size: vk::DeviceSize, usage: vk::BufferUsageFlags, properties: vk::MemoryPropertyFlags, sharing_mode: vk::SharingMode, queue_family_indices: Option<&[u32]>)
    -> Self {

        let mut buffer_info = vk::BufferCreateInfo {
            s_type: vk::StructureType::BUFFER_CREATE_INFO,
            p_next: ptr::null(),
            flags: Default::default(),
            size,
            usage,
            sharing_mode,
            queue_family_index_count: 0,
            p_queue_family_indices: ptr::null(),
        };

        if let Some(indices) = queue_family_indices {
            buffer_info.queue_family_index_count = indices.len() as u32;
            buffer_info.p_queue_family_indices = indices.as_ptr();
        }

        let buffer = unsafe { device.create_buffer(&buffer_info, None) }
            .expect("failed to create vertex-buffer");

        let mem_requirements = unsafe { device.get_buffer_memory_requirements(buffer) };


        let mem_type_idx = find_memory_type(
            instance,
            phys_device,
            mem_requirements.memory_type_bits,
            properties,
        );

        let alloc_info = vk::MemoryAllocateInfo {
            s_type: vk::StructureType::MEMORY_ALLOCATE_INFO,
            p_next: ptr::null(),
            allocation_size: mem_requirements.size,
            memory_type_index: mem_type_idx
        };

        let mem = unsafe { device.allocate_memory(&alloc_info, None) }
            .expect("failed to allocate vertex memory");

        unsafe { device.bind_buffer_memory(buffer, mem, 0) }
            .expect("failed to bind vertex-buffer");



        Self {
            buffer,
            mem,
            size
        }
    }

    pub fn copy_buffer(device: &ash::Device, cmd_pool: vk::CommandPool, queue: vk::Queue, src_buff: &BufferData, dst_buff: &BufferData) {
        let command_buffer = buffer_utils::begin_single_time_commands(device, cmd_pool);

        let copy_region = vk::BufferCopy {
            src_offset: 0,
            dst_offset: 0,
            size: src_buff.size,
        };

        unsafe {
            device.cmd_copy_buffer(command_buffer, src_buff.buffer, dst_buff.buffer, &[copy_region]);
        }

        buffer_utils::end_single_time_commands(device, cmd_pool, command_buffer, queue);
    }
}



pub fn find_memory_type(instance: &ash::Instance, phys_device: vk::PhysicalDevice, type_filter: u32, properties: vk::MemoryPropertyFlags) -> u32 {
    let mem_props = unsafe { instance.get_physical_device_memory_properties(phys_device) };

    for (i, mem_type) in mem_props.memory_types.iter().enumerate() {
        if (type_filter & (1 << i) > 0) && mem_type.property_flags.contains(properties)  {
            return i as u32;
        }
    }

    panic!("failed to find suitable memory-type");
}

