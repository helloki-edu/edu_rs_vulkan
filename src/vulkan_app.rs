use crate::app::BufferData;
use crate::device_analysis::{self, DeviceSuitability};
use crate::queue_family_support::QueueFamilySupport;
use crate::utils::img_utils::{self, ImgData};
use crate::utils::model::Model;
use crate::utils::validation_layers::check_validation_layer_support;
use crate::vertex::Vertex;
use crate::{app, cfg, utils};
use ash::vk;
use cgmath::{Matrix4, Point3, SquareMatrix, Vector3};
use std::ffi::{CStr, CString};
use std::io::Read;
use std::os::raw::c_void;
use std::path::Path;
use std::{fs, mem, ptr};
use winit::event::{ElementState, Event, KeyboardInput, VirtualKeyCode, WindowEvent};
use winit::event_loop::{ControlFlow, EventLoop};

unsafe extern "system" fn vulkan_debut_utils_callback(
    message_severity: vk::DebugUtilsMessageSeverityFlagsEXT,
    message_type: vk::DebugUtilsMessageTypeFlagsEXT,
    p_callback_data: *const vk::DebugUtilsMessengerCallbackDataEXT,
    _p_user_data: *mut c_void,
) -> vk::Bool32 {
    let severity = match message_severity {
        vk::DebugUtilsMessageSeverityFlagsEXT::VERBOSE => "[VERBOSE]",
        vk::DebugUtilsMessageSeverityFlagsEXT::WARNING => "[WARNING]",
        vk::DebugUtilsMessageSeverityFlagsEXT::ERROR => "[ERROR  ]",
        vk::DebugUtilsMessageSeverityFlagsEXT::INFO => "[INFO   ]",
        _ => "[UNKNOWN]",
    };

    let types = match message_type {
        vk::DebugUtilsMessageTypeFlagsEXT::GENERAL => "[GENERAL    ]",
        vk::DebugUtilsMessageTypeFlagsEXT::PERFORMANCE => "[PERFORMANCE]",
        vk::DebugUtilsMessageTypeFlagsEXT::VALIDATION => "[VALIDATION ]",
        _ => "[UNKNOWN    ]",
    };

    let message = CStr::from_ptr((*p_callback_data).p_message);

    println!("[DEBUG]{severity}{types}{message:?}");

    // vulkan call that triggered validation-layer messsage should be aborted?
    vk::FALSE
}

#[repr(C)]
#[derive(Debug, Clone)]
struct UniformBufferObject {
    model: Matrix4<f32>,
    view: Matrix4<f32>,
    proj: Matrix4<f32>,
}

struct SyncData {
    img_available_sem: vk::Semaphore,
    render_finished_sem: vk::Semaphore,
    in_flight_fence: vk::Fence,
}

struct PipelineData {
    pipeline_layout: vk::PipelineLayout,
    graphics_pipeline: vk::Pipeline,
}

pub struct SurfaceData {
    pub surface: vk::SurfaceKHR,
    pub surface_loader: ash::extensions::khr::Surface,
}

struct DebugData {
    debug_utils_loader: ash::extensions::ext::DebugUtils,
    debug_messenger: vk::DebugUtilsMessengerEXT,
}

struct QueueData {
    graphics_idx: u32,
    graphics_queue: vk::Queue,
    _present_idx: u32,
    present_queue: vk::Queue,
    transfer_idx: u32,
    transfer_queue: vk::Queue,
}

impl QueueData {
    fn from_support(device: &ash::Device, s: QueueFamilySupport) -> Self {
        let graphics_idx = s.graphics_family.unwrap();
        let present_idx = s.present_family.unwrap();
        let transfer_idx = s.transfer_family.unwrap();

        unsafe {
            Self {
                graphics_idx,
                graphics_queue: device.get_device_queue(graphics_idx, 0),
                _present_idx: present_idx,
                present_queue: device.get_device_queue(present_idx, 0),
                transfer_idx,
                transfer_queue: device.get_device_queue(transfer_idx, 0),
            }
        }
    }
}

pub struct VulkanApp {
    window: winit::window::Window,

    _entry: ash::Entry,
    instance: ash::Instance,
    surface_data: SurfaceData,
    debug_data: DebugData,
    _phys_device: vk::PhysicalDevice,
    device: ash::Device,
    queue_data: QueueData,
    swapchain_data: app::SwapchainData,

    render_pass: vk::RenderPass,
    descriptor_set_layout: vk::DescriptorSetLayout,
    pipeline_data: PipelineData,
    framebuffers: Vec<vk::Framebuffer>,

    graphics_cmd_pool: vk::CommandPool,
    transfer_cmd_pool: vk::CommandPool,

    graphics_cmd_buffers: Vec<vk::CommandBuffer>,

    // TODO: use same buffer for both data
    vertex_data: app::BufferData,
    index_data: app::BufferData,

    uniform_data: Vec<app::BufferData>,
    uniform_transform: UniformBufferObject,

    descriptor_pool: vk::DescriptorPool,
    descriptor_sets: Vec<vk::DescriptorSet>,

    texture_data: ImgData,
    texture_sampler: vk::Sampler,

    // msaa
    color_data: ImgData,

    depth_data: ImgData,

    sync_data: Vec<SyncData>,

    curr_frame: u32,

    model: Model,

    _msaa_samples: vk::SampleCountFlags,
}

// init window
impl VulkanApp {
    fn create_window(event_loop: &EventLoop<()>) -> winit::window::Window {
        winit::window::WindowBuilder::new()
            .with_title(cfg::WINDOW_TITLE)
            .with_inner_size(winit::dpi::LogicalSize::new(
                cfg::WINDOW_WIDTH,
                cfg::WINDOW_HEIGHT,
            ))
            .build(event_loop)
            .expect("Failed to create window")
    }
}

// init vulkan
impl VulkanApp {
    pub fn new(event_loop: &winit::event_loop::EventLoop<()>) -> Self {
        dbg!("VulkanApp - new: creating new app...");

        let window = Self::create_window(event_loop);
        let entry = unsafe { ash::Entry::load().unwrap() };
        let instance = Self::create_instance(&entry);
        let surface_data = Self::create_surface(&entry, &instance, &window);
        let debug_data = Self::setup_debug_messanger(&entry, &instance);
        let phys_device = Self::pick_physical_device(&instance, &surface_data);
        let msaa_samples = device_analysis::get_max_useable_sample_count(&instance, phys_device);
        let (device, queue_data) =
            Self::create_logical_device(&instance, phys_device, &surface_data);
        let swapchain_data =
            app::SwapchainData::new(&instance, phys_device, &device, &surface_data);

        let graphics_cmd_pool = Self::create_command_pool(
            &device,
            queue_data.graphics_idx,
            vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER,
        );
        let transfer_cmd_pool = Self::create_command_pool(
            &device,
            queue_data.transfer_idx,
            vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER
                | vk::CommandPoolCreateFlags::TRANSIENT,
        );

        let color_data = Self::create_color_resources(
            &instance,
            &device,
            phys_device,
            swapchain_data.surface_format.format,
            swapchain_data.extent,
            msaa_samples,
        );

        let depth_data = Self::create_depth_resources(
            &instance,
            &device,
            phys_device,
            graphics_cmd_pool,
            queue_data.graphics_queue,
            swapchain_data.extent.width,
            swapchain_data.extent.height,
            msaa_samples,
        );

        let render_pass = Self::create_render_pass(
            &instance,
            &device,
            phys_device,
            swapchain_data.surface_format.format,
            msaa_samples,
        );
        let descriptor_set_layout = Self::create_descriptor_set_layout(&device);
        let pipeline_data = Self::create_graphics_pipeline(
            &device,
            &swapchain_data.extent,
            render_pass,
            &descriptor_set_layout,
            msaa_samples
        );
        let framebuffers = Self::create_framebuffers(
            &device,
            &swapchain_data.extent,
            &swapchain_data.img_views,
            depth_data.view,
            color_data.view,
            render_pass,
        );

        let graphics_cmd_buffers = Self::create_command_buffers(&device, graphics_cmd_pool);

        let model = Model::new(Path::new(cfg::VIKING_MODEL_PATH));

        let vertex_data = Self::create_vertex_buffer(
            &instance,
            &device,
            phys_device,
            transfer_cmd_pool,
            &queue_data,
            &model.vertices,
        );
        let index_data = Self::create_index_buffer(
            &instance,
            &device,
            phys_device,
            transfer_cmd_pool,
            &queue_data,
            &model.indices,
        );

        let uniform_data = Self::create_uniform_buffers(&instance, &device, phys_device);

        // let texture_data = Self::create_texture_img(
        //     &instance,
        //     &device,
        //     phys_device,
        //     graphics_cmd_pool,
        //     queue_data.graphics_queue,
        //     std::path::Path::new("textures/texture.jpg"),
        // );
        let texture_data = Self::create_texture_img(
            &instance,
            &device,
            phys_device,
            graphics_cmd_pool,
            queue_data.graphics_queue,
            std::path::Path::new(cfg::VIKING_TEXTURE_PATH),
        );
        let texture_sampler = img_utils::create_texture_sampler(
            &instance,
            &device,
            phys_device,
            texture_data.mip_levels,
        );

        let descriptor_pool = Self::create_descriptor_pool(&device);
        let descriptor_sets = Self::create_descriptor_sets(
            &device,
            descriptor_pool,
            descriptor_set_layout,
            swapchain_data.img_views.len(),
            &uniform_data,
            texture_sampler,
            texture_data.view,
        );

        let sync_data = Self::create_sync_data(&device);

        let aspect_ratio = swapchain_data.extent.width as f32 / swapchain_data.extent.height as f32;
        let uniform_transform = UniformBufferObject {
            model: Matrix4::<f32>::identity(),
            view: Matrix4::look_at_rh(
                Point3::new(2.0, 2.0, 2.0),
                Point3::new(0.0, 0.0, 0.0),
                Vector3::new(0.0, 0.0, 1.0),
            ),
            proj: {
                let mut proj = cgmath::perspective(cgmath::Deg(45.0), aspect_ratio, 0.1, 10.0);
                proj[1][1] *= -1.0;
                proj
            },
        };

        VulkanApp {
            window,

            _entry: entry,
            instance,
            surface_data,
            debug_data,
            _phys_device: phys_device,
            device,
            queue_data,
            swapchain_data,

            render_pass,
            descriptor_set_layout,
            pipeline_data,
            framebuffers,

            graphics_cmd_pool,
            graphics_cmd_buffers,

            transfer_cmd_pool,

            vertex_data,
            index_data,

            uniform_data,
            uniform_transform,

            descriptor_pool,
            descriptor_sets,

            texture_data,
            texture_sampler,

            color_data,

            depth_data,

            sync_data,

            curr_frame: 0,

            model,

            _msaa_samples: msaa_samples,
        }
    }

    fn create_instance(entry: &ash::Entry) -> ash::Instance {
        if cfg::ENABLE_VALIDATION_LAYERS
            && !check_validation_layer_support(entry, cfg::VALIDATION_LAYER_NAMES)
        {
            panic!("validation layers requested, but not available!");
        }

        let app_name = CString::new(cfg::WINDOW_TITLE).unwrap();
        let engine_name = CString::new(cfg::ENGINE_NAME).unwrap();

        let app_info = vk::ApplicationInfo {
            s_type: vk::StructureType::APPLICATION_INFO,
            p_next: ptr::null(),
            p_application_name: app_name.as_ptr(),
            application_version: cfg::APPLICATION_VERSION,
            p_engine_name: engine_name.as_ptr(),
            engine_version: cfg::ENGINE_VERSION,
            api_version: cfg::API_VERSION,
        };

        let extension_names = utils::platforms::required_extension_names();

        let mut create_info = vk::InstanceCreateInfo {
            s_type: vk::StructureType::INSTANCE_CREATE_INFO,
            p_next: ptr::null(),
            flags: vk::InstanceCreateFlags::empty(),
            p_application_info: &app_info,
            enabled_layer_count: 0,
            pp_enabled_layer_names: ptr::null(),
            enabled_extension_count: extension_names.len() as u32,
            pp_enabled_extension_names: extension_names.as_ptr(),
        };

        let enabled_validation_layers: Vec<_> = cfg::VALIDATION_LAYER_NAMES
            .iter()
            .map(|l| CString::new(*l).unwrap())
            .collect();

        let enabled_validation_layer_ref: Vec<_> = enabled_validation_layers
            .iter()
            .map(|l| l.as_ptr())
            .collect();

        // This create info used to debug issues in vk::createInstance and vk::destroyInstance.
        let debug_utils_create_info = Self::populate_debug_messenger_create_info();

        // add validation layers
        if cfg::ENABLE_VALIDATION_LAYERS {
            create_info.enabled_layer_count = cfg::VALIDATION_LAYER_NAMES.len() as u32;
            create_info.pp_enabled_layer_names = enabled_validation_layer_ref.as_ptr();

            create_info.p_next = &debug_utils_create_info
                as *const vk::DebugUtilsMessengerCreateInfoEXT
                as *const c_void;
        }

        unsafe {
            entry
                .create_instance(&create_info, None)
                .expect("failed to create instance")
        }
    }

    fn create_surface(
        entry: &ash::Entry,
        instance: &ash::Instance,
        window: &winit::window::Window,
    ) -> SurfaceData {
        let surface = unsafe {
            utils::platforms::create_surface(entry, instance, window)
                .expect("failed to create surface")
        };

        let surface_loader = ash::extensions::khr::Surface::new(entry, instance);

        SurfaceData {
            surface,
            surface_loader,
        }
    }

    fn pick_physical_device(
        instance: &ash::Instance,
        surface_data: &SurfaceData,
    ) -> vk::PhysicalDevice {
        let all_devices = unsafe { instance.enumerate_physical_devices().unwrap() };

        let device = match all_devices
            .into_iter()
            .map(|d| {
                DeviceSuitability::rate_device_suitability(
                    instance,
                    d,
                    surface_data,
                    cfg::DEVICE_EXTENSION_NAMES,
                )
            })
            .filter(|d| d.score > 0)
            .max_by(|r1, r2| r1.score.cmp(&r2.score))
        {
            None => {
                panic!("no suitable physical device found");
            }
            Some(d) => d,
        };

        println!("physical device picked: {:?}", device);

        device.device
    }

    fn create_logical_device(
        instance: &ash::Instance,
        phys_device: vk::PhysicalDevice,
        surface_data: &SurfaceData,
    ) -> (ash::Device, QueueData) {
        let queue_families = QueueFamilySupport::new(instance, phys_device, surface_data);
        let queue_priorities = [1.0_f32];

        let queue_create_infos: Vec<_> = queue_families
            .indices()
            .into_iter()
            .map(|idx| vk::DeviceQueueCreateInfo {
                s_type: vk::StructureType::DEVICE_QUEUE_CREATE_INFO,
                p_next: ptr::null(),
                flags: vk::DeviceQueueCreateFlags::empty(),
                queue_family_index: idx,
                queue_count: 1,
                p_queue_priorities: queue_priorities.as_ptr(),
            })
            .collect();

        let phys_device_features = vk::PhysicalDeviceFeatures::builder()
            .sampler_anisotropy(true)
            .sample_rate_shading(cfg::ENABLE_SAMPLE_SHADING)   // Sample-Shading
            .build();

        let extension_names: Vec<_> = cfg::DEVICE_EXTENSION_NAMES
            .iter()
            .map(|n| CString::new(*n).unwrap())
            .collect();

        let extension_names_input: Vec<_> = extension_names.iter().map(|n| n.as_ptr()).collect();

        let mut logic_device_create_info = vk::DeviceCreateInfo {
            s_type: vk::StructureType::DEVICE_CREATE_INFO,
            p_next: ptr::null(),
            flags: vk::DeviceCreateFlags::empty(),
            queue_create_info_count: queue_create_infos.len() as u32,
            p_queue_create_infos: queue_create_infos.as_ptr(),
            // layers: ignored by modern implementations - add anyway, for backwards compatibility
            enabled_layer_count: 0,
            pp_enabled_layer_names: ptr::null(),
            // extensions: device-specific
            enabled_extension_count: cfg::DEVICE_EXTENSION_NAMES.len() as u32,
            pp_enabled_extension_names: extension_names_input.as_ptr(),
            p_enabled_features: &phys_device_features,
        };

        let validation_layer_names: Vec<_> = cfg::VALIDATION_LAYER_NAMES
            .iter()
            .map(|l| CString::new(*l).unwrap())
            .collect();

        let validation_layer_names_input: Vec<_> =
            validation_layer_names.iter().map(|l| l.as_ptr()).collect();

        if cfg::ENABLE_VALIDATION_LAYERS {
            logic_device_create_info.enabled_layer_count =
                validation_layer_names_input.len() as u32;
            logic_device_create_info.pp_enabled_layer_names = validation_layer_names_input.as_ptr();
        }

        let device = unsafe {
            instance
                .create_device(phys_device, &logic_device_create_info, None)
                .expect("failed to create logical device")
        };

        let queue_data = QueueData::from_support(&device, queue_families);

        (device, queue_data)
    }
}

impl VulkanApp {
    fn setup_debug_messanger(entry: &ash::Entry, instance: &ash::Instance) -> DebugData {
        let debug_utils_loader = ash::extensions::ext::DebugUtils::new(entry, instance);

        if !cfg::ENABLE_VALIDATION_LAYERS {
            DebugData {
                debug_utils_loader,
                debug_messenger: vk::DebugUtilsMessengerEXT::null(),
            }
        } else {
            let messenger_ci = Self::populate_debug_messenger_create_info();
            let debug_messenger = unsafe {
                debug_utils_loader
                    .create_debug_utils_messenger(&messenger_ci, None)
                    .expect("debug utils callback")
            };

            DebugData {
                debug_utils_loader,
                debug_messenger,
            }
        }
    }

    fn populate_debug_messenger_create_info() -> vk::DebugUtilsMessengerCreateInfoEXT {
        vk::DebugUtilsMessengerCreateInfoEXT {
            s_type: vk::StructureType::DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
            p_next: ptr::null(),
            flags: vk::DebugUtilsMessengerCreateFlagsEXT::empty(),
            message_severity:
            // vk::DebugUtilsMessageSeverityFlagsEXT::VERBOSE |
            // vk::DebugUtilsMessageSeverityFlagsEXT::INFO |
            vk::DebugUtilsMessageSeverityFlagsEXT::WARNING |
                vk::DebugUtilsMessageSeverityFlagsEXT::ERROR,
            message_type:
            vk::DebugUtilsMessageTypeFlagsEXT::GENERAL |
                vk::DebugUtilsMessageTypeFlagsEXT::PERFORMANCE |
                vk::DebugUtilsMessageTypeFlagsEXT::VALIDATION,
            pfn_user_callback: Some(vulkan_debut_utils_callback),
            p_user_data: ptr::null_mut()
        }
    }

    fn create_descriptor_set_layout(device: &ash::Device) -> vk::DescriptorSetLayout {
        let ubo_layout_binding = vk::DescriptorSetLayoutBinding {
            binding: 0,
            descriptor_type: vk::DescriptorType::UNIFORM_BUFFER,
            descriptor_count: 1,
            stage_flags: vk::ShaderStageFlags::VERTEX,
            p_immutable_samplers: ptr::null(),
        };

        let sampler_layout_binding = vk::DescriptorSetLayoutBinding {
            binding: 1,
            descriptor_type: vk::DescriptorType::COMBINED_IMAGE_SAMPLER,
            descriptor_count: 1,
            stage_flags: vk::ShaderStageFlags::FRAGMENT,
            p_immutable_samplers: ptr::null(),
        };

        let bindings = [ubo_layout_binding, sampler_layout_binding];

        let layout_info = vk::DescriptorSetLayoutCreateInfo {
            s_type: vk::StructureType::DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
            p_next: ptr::null(),
            flags: vk::DescriptorSetLayoutCreateFlags::empty(),
            binding_count: bindings.len() as u32,
            p_bindings: bindings.as_ptr(),
        };

        unsafe {
            device
                .create_descriptor_set_layout(&layout_info, None)
                .expect("Failed to create descriptor-set-layout")
        }
    }

    fn create_graphics_pipeline(
        device: &ash::Device,
        swap_extent: &vk::Extent2D,
        render_pass: vk::RenderPass,
        descriptor_set_layout: &vk::DescriptorSetLayout,
        msaa_samples: vk::SampleCountFlags
    ) -> PipelineData {
        let vert_code = Self::read_shader_code(Path::new(cfg::VERT_SHADER_PATH));
        println!("vertex-shader loaded: {} bytes", vert_code.len());
        let frag_code = Self::read_shader_code(Path::new(cfg::FRAG_SHADER_PATH));
        println!("frag-shader loaded: {} bytes", frag_code.len());

        let vert_module = Self::create_shader_module(device, &vert_code);
        let frag_module = Self::create_shader_module(device, &frag_code);

        let entrypoint = CString::new("main").unwrap();

        let shader_stages = [
            // vert-shader
            vk::PipelineShaderStageCreateInfo {
                s_type: vk::StructureType::PIPELINE_SHADER_STAGE_CREATE_INFO,
                p_next: ptr::null(),
                flags: vk::PipelineShaderStageCreateFlags::empty(),
                stage: vk::ShaderStageFlags::VERTEX,
                module: vert_module,
                p_name: entrypoint.as_ptr(),
                p_specialization_info: ptr::null(),
            },
            // frag-shader
            vk::PipelineShaderStageCreateInfo {
                s_type: vk::StructureType::PIPELINE_SHADER_STAGE_CREATE_INFO,
                p_next: ptr::null(),
                flags: vk::PipelineShaderStageCreateFlags::empty(),
                stage: vk::ShaderStageFlags::FRAGMENT,
                module: frag_module,
                p_name: entrypoint.as_ptr(),
                p_specialization_info: ptr::null(),
            },
        ];

        let vert_binding_desc = Vertex::get_binding_desc();
        let vert_attr_desc = Vertex::get_attribute_desc();

        let vertex_input_create_info = vk::PipelineVertexInputStateCreateInfo {
            s_type: vk::StructureType::PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
            p_next: ptr::null(),
            flags: vk::PipelineVertexInputStateCreateFlags::empty(),
            vertex_binding_description_count: 1,
            p_vertex_binding_descriptions: &vert_binding_desc,
            vertex_attribute_description_count: vert_attr_desc.len() as u32,
            p_vertex_attribute_descriptions: vert_attr_desc.as_ptr(),
        };

        let input_assembly = vk::PipelineInputAssemblyStateCreateInfo {
            s_type: vk::StructureType::PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
            p_next: ptr::null(),
            flags: vk::PipelineInputAssemblyStateCreateFlags::empty(),
            topology: vk::PrimitiveTopology::TRIANGLE_LIST,
            primitive_restart_enable: vk::FALSE,
        };

        let viewport = vk::Viewport {
            x: 0.0,
            y: 0.0,
            width: swap_extent.width as f32,
            height: swap_extent.height as f32,
            min_depth: 0.0,
            max_depth: 1.0,
        };

        let scissor = vk::Rect2D {
            offset: vk::Offset2D { x: 0, y: 0 },
            extent: *swap_extent,
        };

        let viewport_state_create_info = vk::PipelineViewportStateCreateInfo {
            s_type: vk::StructureType::PIPELINE_VIEWPORT_STATE_CREATE_INFO,
            p_next: ptr::null(),
            flags: vk::PipelineViewportStateCreateFlags::empty(),
            viewport_count: 1,
            p_viewports: &viewport,
            scissor_count: 1,
            p_scissors: &scissor,
        };

        let rasterization_state_create_info = vk::PipelineRasterizationStateCreateInfo {
            s_type: vk::StructureType::PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
            p_next: ptr::null(),
            flags: vk::PipelineRasterizationStateCreateFlags::empty(),
            depth_clamp_enable: vk::FALSE, // clamp fragments that are beyond the near- and far-plane to them
            rasterizer_discard_enable: vk::FALSE, // prevents geometry to pass through the rasterizer stage
            polygon_mode: vk::PolygonMode::FILL,
            cull_mode: vk::CullModeFlags::BACK,
            front_face: vk::FrontFace::COUNTER_CLOCKWISE,
            depth_bias_enable: vk::FALSE,
            depth_bias_constant_factor: 0.0,
            depth_bias_clamp: 0.0,
            depth_bias_slope_factor: 0.0,
            line_width: 1.0,
        };

        // todo: depth and stencil testing
        let multisampling_create_info = vk::PipelineMultisampleStateCreateInfo {
            s_type: vk::StructureType::PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
            p_next: ptr::null(),
            flags: vk::PipelineMultisampleStateCreateFlags::empty(),
            rasterization_samples: msaa_samples,
            sample_shading_enable: cfg::ENABLE_SAMPLE_SHADING as u32,
            min_sample_shading: cfg::MIN_SAMPLE_SHADING,
            p_sample_mask: ptr::null(),
            alpha_to_coverage_enable: vk::FALSE,
            alpha_to_one_enable: vk::FALSE,
        };

        // config per attached framebuffer (here 1)
        let color_blend_attachment = vk::PipelineColorBlendAttachmentState {
            blend_enable: vk::FALSE,
            src_color_blend_factor: vk::BlendFactor::SRC_ALPHA,
            dst_color_blend_factor: vk::BlendFactor::ONE_MINUS_SRC_ALPHA,
            color_blend_op: vk::BlendOp::ADD,
            src_alpha_blend_factor: vk::BlendFactor::ONE,
            dst_alpha_blend_factor: vk::BlendFactor::ZERO,
            alpha_blend_op: vk::BlendOp::ADD,
            color_write_mask: vk::ColorComponentFlags::RGBA,
        };

        let color_blending_create_info = vk::PipelineColorBlendStateCreateInfo {
            s_type: vk::StructureType::PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
            p_next: ptr::null(),
            flags: vk::PipelineColorBlendStateCreateFlags::empty(),
            logic_op_enable: vk::FALSE,
            logic_op: vk::LogicOp::COPY,
            attachment_count: 1,
            p_attachments: &color_blend_attachment,
            blend_constants: [0.0, 0.0, 0.0, 0.0],
        };

        // change stuff without having to recreate the pipeline
        // let dyn_state = [
        //     vk::DynamicState::VIEWPORT,
        //     vk::DynamicState::LINE_WIDTH,
        // ];
        //
        // let dyn_state_create_info = vk::PipelineDynamicStateCreateInfo {
        //     s_type: vk::StructureType::PIPELINE_DYNAMIC_STATE_CREATE_INFO,
        //     p_next: ptr::null(),
        //     flags: vk::PipelineDynamicStateCreateFlags::empty(),
        //     dynamic_state_count: dyn_state.len() as u32,
        //     p_dynamic_states: dyn_state.as_ptr()
        // };

        // used to add uniforms
        let pipeline_layout_create_info = vk::PipelineLayoutCreateInfo {
            s_type: vk::StructureType::PIPELINE_LAYOUT_CREATE_INFO,
            p_next: ptr::null(),
            flags: vk::PipelineLayoutCreateFlags::empty(),
            set_layout_count: 1,
            p_set_layouts: descriptor_set_layout,
            push_constant_range_count: 0,
            p_push_constant_ranges: ptr::null(),
        };

        let pipeline_layout = unsafe {
            device
                .create_pipeline_layout(&pipeline_layout_create_info, None)
                .expect("failed to create pipeline layout")
        };

        // depth
        let depth_stencil = vk::PipelineDepthStencilStateCreateInfo {
            s_type: vk::StructureType::PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
            p_next: ptr::null(),
            flags: vk::PipelineDepthStencilStateCreateFlags::empty(),
            depth_test_enable: vk::TRUE, // check depth of new fragment against value in buffer
            depth_write_enable: vk::TRUE, // write new value to buffer if check passes
            depth_compare_op: vk::CompareOp::LESS,
            depth_bounds_test_enable: vk::FALSE,
            stencil_test_enable: vk::FALSE, // optional depth-bound-test (only keep fragments that fall within the specified depth range)
            min_depth_bounds: 0.0,
            max_depth_bounds: 1.0,
            front: vk::StencilOpState::default(),
            back: vk::StencilOpState::default(),
        };

        let pipeline_info = vk::GraphicsPipelineCreateInfo {
            s_type: vk::StructureType::GRAPHICS_PIPELINE_CREATE_INFO,
            p_next: ptr::null(),
            flags: Default::default(),
            stage_count: shader_stages.len() as u32,
            p_stages: shader_stages.as_ptr(),
            p_vertex_input_state: &vertex_input_create_info,
            p_input_assembly_state: &input_assembly,
            p_tessellation_state: ptr::null(),
            p_viewport_state: &viewport_state_create_info,
            p_rasterization_state: &rasterization_state_create_info,
            p_multisample_state: &multisampling_create_info,
            p_depth_stencil_state: &depth_stencil, // must always be specified, if the render pass contains a depth stencil attachment
            p_color_blend_state: &color_blending_create_info,
            p_dynamic_state: ptr::null(),
            layout: pipeline_layout,
            render_pass,
            subpass: 0,
            base_pipeline_handle: vk::Pipeline::null(),
            base_pipeline_index: -1,
        };

        let graphics_pipeline = unsafe {
            device
                .create_graphics_pipelines(vk::PipelineCache::null(), &[pipeline_info], None)
                .expect("failed to create pipeline")[0]
        };

        unsafe {
            device.destroy_shader_module(vert_module, None);
            device.destroy_shader_module(frag_module, None);
        }

        PipelineData {
            pipeline_layout,
            graphics_pipeline,
        }
    }

    fn read_shader_code(path: &Path) -> Vec<u8> {
        let spv_file = fs::File::open(path)
            .unwrap_or_else(|_| panic!("failed to open spv file: '{:?}'", path));

        spv_file.bytes().filter_map(|b| b.ok()).collect()
    }

    fn create_shader_module(device: &ash::Device, code: &[u8]) -> vk::ShaderModule {
        let create_info = vk::ShaderModuleCreateInfo {
            s_type: vk::StructureType::SHADER_MODULE_CREATE_INFO,
            p_next: ptr::null(),
            flags: vk::ShaderModuleCreateFlags::empty(),
            code_size: code.len(),
            p_code: code.as_ptr() as *const u32,
        };

        unsafe {
            device
                .create_shader_module(&create_info, None)
                .expect("failed to create shader module")
        }
    }

    fn create_render_pass(
        instance: &ash::Instance,
        device: &ash::Device,
        phys_device: vk::PhysicalDevice,
        swap_format: vk::Format,
        msaa_samples: vk::SampleCountFlags,
    ) -> vk::RenderPass {
        // color
        // -----
        let color_attachment = vk::AttachmentDescription {
            flags: Default::default(),
            format: swap_format,
            samples: msaa_samples,
            load_op: vk::AttachmentLoadOp::CLEAR,
            store_op: vk::AttachmentStoreOp::STORE,
            stencil_load_op: vk::AttachmentLoadOp::DONT_CARE,
            stencil_store_op: vk::AttachmentStoreOp::DONT_CARE,
            initial_layout: vk::ImageLayout::UNDEFINED,
            // final_layout: vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL, // without multisampling
            final_layout: vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL, // multisampled cannot be presented directyl -> resolve to a regular image first (does not apply to depth-buffer -> won't be presented)
        };

        let color_attachment_ref = vk::AttachmentReference {
            attachment: 0, // frag-shader -> layout(location = 0)
            layout: vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
        };

        // depth
        // -----
        let depth_attachment = vk::AttachmentDescription {
            flags: vk::AttachmentDescriptionFlags::default(),
            format: Self::find_depth_format(instance, phys_device),
            samples: msaa_samples,
            load_op: vk::AttachmentLoadOp::CLEAR,
            store_op: vk::AttachmentStoreOp::DONT_CARE, // we won't use the data after drawing has finished
            stencil_load_op: vk::AttachmentLoadOp::DONT_CARE,
            stencil_store_op: vk::AttachmentStoreOp::DONT_CARE,
            initial_layout: vk::ImageLayout::UNDEFINED, // we don't care about previous depth contents
            final_layout: vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        };

        let depth_attachment_ref = vk::AttachmentReference {
            attachment: 1,
            layout: vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        };

        // msaa
        let color_attachment_resolve = vk::AttachmentDescription {
            flags: vk::AttachmentDescriptionFlags::default(),
            format: swap_format,
            samples: vk::SampleCountFlags::TYPE_1,
            load_op: vk::AttachmentLoadOp::DONT_CARE,
            store_op: vk::AttachmentStoreOp::STORE,
            stencil_load_op: vk::AttachmentLoadOp::DONT_CARE,
            stencil_store_op: vk::AttachmentStoreOp::DONT_CARE,
            initial_layout: vk::ImageLayout::UNDEFINED,
            final_layout: vk::ImageLayout::PRESENT_SRC_KHR,
        };

        // instruct render-pass to resolve multisampled color image to into regular attachment
        let color_attachment_resolve_ref = vk::AttachmentReference {
            attachment: 2,
            layout: vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
        };

        let subpass = vk::SubpassDescription {
            flags: Default::default(),
            pipeline_bind_point: vk::PipelineBindPoint::GRAPHICS,
            input_attachment_count: 0,
            p_input_attachments: ptr::null(),
            color_attachment_count: 1,
            p_color_attachments: &color_attachment_ref,
            p_resolve_attachments: &color_attachment_resolve_ref,
            p_depth_stencil_attachment: &depth_attachment_ref,
            preserve_attachment_count: 0,
            p_preserve_attachments: ptr::null(),
        };

        // prevent transition from happening before its necessary / allowed
        let dependencies = [vk::SubpassDependency {
            src_subpass: vk::SUBPASS_EXTERNAL, // refers to implicit subpass before, or after the render pass - depending on whether it is specified in src or dst
            dst_subpass: 0,
            // operations to wait on -> wait for the swap-chain to finish reading from the img
            // depth-img is accessed first in early-fragment-test stage
            src_stage_mask: vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT
                | vk::PipelineStageFlags::EARLY_FRAGMENT_TESTS,
            src_access_mask: vk::AccessFlags::empty(),
            // operation that has to wait: writing of the color attachment in the color attachment state
            dst_stage_mask: vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT
                | vk::PipelineStageFlags::EARLY_FRAGMENT_TESTS,
            // depth: we have a load-op that clears -> so we should specify the access-mask for writes
            dst_access_mask: vk::AccessFlags::COLOR_ATTACHMENT_WRITE
                | vk::AccessFlags::DEPTH_STENCIL_ATTACHMENT_WRITE,
            dependency_flags: Default::default(),
        }];

        let attachments = [color_attachment, depth_attachment, color_attachment_resolve];

        let render_pass_ci = vk::RenderPassCreateInfo {
            s_type: vk::StructureType::RENDER_PASS_CREATE_INFO,
            p_next: ptr::null(),
            flags: vk::RenderPassCreateFlags::empty(),
            attachment_count: attachments.len() as u32,
            p_attachments: attachments.as_ptr(),
            subpass_count: 1,
            p_subpasses: &subpass,
            dependency_count: dependencies.len() as u32,
            p_dependencies: dependencies.as_ptr(),
        };

        unsafe { device.create_render_pass(&render_pass_ci, None).unwrap() }
    }

    fn create_framebuffers(
        device: &ash::Device,
        swap_extent: &vk::Extent2D,
        swap_img_views: &[vk::ImageView],
        depth_img_view: vk::ImageView,
        color_img_view: vk::ImageView,
        render_pass: vk::RenderPass,
    ) -> Vec<vk::Framebuffer> {
        swap_img_views
            .iter()
            .map(|sv| {
                let attachments = [color_img_view, depth_img_view, *sv];

                let create_info = vk::FramebufferCreateInfo {
                    s_type: vk::StructureType::FRAMEBUFFER_CREATE_INFO,
                    p_next: ptr::null(),
                    flags: vk::FramebufferCreateFlags::empty(),
                    render_pass,
                    attachment_count: attachments.len() as u32,
                    p_attachments: attachments.as_ptr(),
                    width: swap_extent.width,
                    height: swap_extent.height,
                    layers: 1,
                };

                unsafe {
                    device
                        .create_framebuffer(&create_info, None)
                        .expect("failed to create framebuffer")
                }
            })
            .collect()
    }

    fn create_command_pool(
        device: &ash::Device,
        queue_family_idx: u32,
        flags: vk::CommandPoolCreateFlags,
    ) -> vk::CommandPool {
        let pool_info = vk::CommandPoolCreateInfo {
            s_type: vk::StructureType::COMMAND_POOL_CREATE_INFO,
            p_next: ptr::null(),
            flags,
            queue_family_index: queue_family_idx,
        };

        unsafe {
            device
                .create_command_pool(&pool_info, None)
                .expect("failed to create command pool")
        }
    }

    fn create_command_buffers(
        device: &ash::Device,
        command_pool: vk::CommandPool,
    ) -> Vec<vk::CommandBuffer> {
        let alloc_info = vk::CommandBufferAllocateInfo {
            s_type: vk::StructureType::COMMAND_BUFFER_ALLOCATE_INFO,
            p_next: ptr::null(),
            command_pool,
            level: vk::CommandBufferLevel::PRIMARY,
            command_buffer_count: cfg::MAX_FRAMES_IN_FLIGHT,
        };

        unsafe {
            device
                .allocate_command_buffers(&alloc_info)
                .expect("failed to allocate command buffer")
        }
    }

    fn create_sync_data(device: &ash::Device) -> Vec<SyncData> {
        let semaphore_info = vk::SemaphoreCreateInfo {
            s_type: vk::StructureType::SEMAPHORE_CREATE_INFO,
            p_next: ptr::null(),
            flags: Default::default(),
        };

        let fence_info = vk::FenceCreateInfo {
            s_type: vk::StructureType::FENCE_CREATE_INFO,
            p_next: ptr::null(),
            // create fence in signaled state, so it won't block the first time, when no img was rendered
            flags: vk::FenceCreateFlags::SIGNALED,
        };

        (0..cfg::MAX_FRAMES_IN_FLIGHT)
            .into_iter()
            .map(|_| {
                let img_available_sem = unsafe {
                    device
                        .create_semaphore(&semaphore_info, None)
                        .expect("failed to create semaphore")
                };

                let render_finished_sem = unsafe {
                    device
                        .create_semaphore(&semaphore_info, None)
                        .expect("failed to create semaphore")
                };

                let in_flight_fence = unsafe {
                    device
                        .create_fence(&fence_info, None)
                        .expect("failed to create fence")
                };

                SyncData {
                    img_available_sem,
                    render_finished_sem,
                    in_flight_fence,
                }
            })
            .collect()
    }

    fn create_vertex_buffer(
        instance: &ash::Instance,
        device: &ash::Device,
        phys_device: vk::PhysicalDevice,
        cmd_pool: vk::CommandPool,
        queue_data: &QueueData,
        vertices: &[Vertex],
    ) -> BufferData {
        let buffer_size = mem::size_of_val(vertices) as vk::DeviceSize;

        let staging_buffer = BufferData::new(
            instance,
            device,
            phys_device,
            buffer_size,
            vk::BufferUsageFlags::TRANSFER_SRC,
            vk::MemoryPropertyFlags::HOST_VISIBLE | vk::MemoryPropertyFlags::HOST_COHERENT,
            vk::SharingMode::EXCLUSIVE,
            None,
        );

        unsafe {
            let mem_ptr = device
                .map_memory(
                    staging_buffer.mem,
                    0,
                    buffer_size,
                    vk::MemoryMapFlags::empty(),
                )
                .expect("failed to map memory") as *mut Vertex;

            // either flush memory, or use host-coherent heap
            // transfer to the GPU happens in the background - is guaranteed to be completed by the next call to vkSubmitQueue
            mem_ptr.copy_from_nonoverlapping(vertices.as_ptr(), vertices.len());

            device.unmap_memory(staging_buffer.mem);
        }

        let device_buffer = BufferData::new(
            instance,
            device,
            phys_device,
            buffer_size,
            vk::BufferUsageFlags::TRANSFER_DST | vk::BufferUsageFlags::VERTEX_BUFFER,
            vk::MemoryPropertyFlags::DEVICE_LOCAL,
            vk::SharingMode::CONCURRENT, // TODO: not optimal
            Some(&[queue_data.graphics_idx, queue_data.transfer_idx]),
        );

        BufferData::copy_buffer(
            device,
            cmd_pool,
            queue_data.transfer_queue,
            &staging_buffer,
            &device_buffer,
        );

        unsafe {
            device.destroy_buffer(staging_buffer.buffer, None);
            device.free_memory(staging_buffer.mem, None);
        }

        device_buffer
    }

    fn create_index_buffer(
        instance: &ash::Instance,
        device: &ash::Device,
        phys_device: vk::PhysicalDevice,
        cmd_pool: vk::CommandPool,
        queue_data: &QueueData,
        indices: &[u32],
    ) -> BufferData {
        let buffer_size = mem::size_of_val(indices) as vk::DeviceSize;

        let staging_buffer = BufferData::new(
            instance,
            device,
            phys_device,
            buffer_size,
            vk::BufferUsageFlags::TRANSFER_SRC,
            vk::MemoryPropertyFlags::HOST_VISIBLE | vk::MemoryPropertyFlags::HOST_COHERENT,
            vk::SharingMode::EXCLUSIVE,
            None,
        );

        unsafe {
            let mem_ptr = device
                .map_memory(
                    staging_buffer.mem,
                    0,
                    buffer_size,
                    vk::MemoryMapFlags::empty(),
                )
                .expect("failed to map index-buffer memory") as *mut u32;

            mem_ptr.copy_from_nonoverlapping(indices.as_ptr(), indices.len());

            device.unmap_memory(staging_buffer.mem);
        }

        let device_buffer = BufferData::new(
            instance,
            device,
            phys_device,
            buffer_size,
            vk::BufferUsageFlags::TRANSFER_DST | vk::BufferUsageFlags::INDEX_BUFFER,
            vk::MemoryPropertyFlags::DEVICE_LOCAL,
            vk::SharingMode::CONCURRENT,
            Some(&[queue_data.graphics_idx, queue_data.transfer_idx]),
        );

        BufferData::copy_buffer(
            device,
            cmd_pool,
            queue_data.transfer_queue,
            &staging_buffer,
            &device_buffer,
        );

        unsafe {
            device.destroy_buffer(staging_buffer.buffer, None);
            device.free_memory(staging_buffer.mem, None);
        }

        device_buffer
    }

    fn create_uniform_buffers(
        instance: &ash::Instance,
        device: &ash::Device,
        phys_device: vk::PhysicalDevice,
    ) -> Vec<app::BufferData> {
        let buffer_size = std::mem::size_of::<UniformBufferObject>() as vk::DeviceSize;

        (0..cfg::MAX_FRAMES_IN_FLIGHT)
            .into_iter()
            .map(|_| {
                BufferData::new(
                    instance,
                    device,
                    phys_device,
                    buffer_size,
                    vk::BufferUsageFlags::UNIFORM_BUFFER,
                    vk::MemoryPropertyFlags::HOST_VISIBLE | vk::MemoryPropertyFlags::HOST_COHERENT,
                    vk::SharingMode::EXCLUSIVE,
                    None,
                )
            })
            .collect()
    }

    fn create_descriptor_pool(device: &ash::Device) -> vk::DescriptorPool {
        let pool_sizes = [
            // uniform buffer
            vk::DescriptorPoolSize {
                ty: vk::DescriptorType::UNIFORM_BUFFER,
                descriptor_count: cfg::MAX_FRAMES_IN_FLIGHT,
            },
            // sampler
            vk::DescriptorPoolSize {
                ty: vk::DescriptorType::COMBINED_IMAGE_SAMPLER,
                descriptor_count: cfg::MAX_FRAMES_IN_FLIGHT,
            },
        ];

        let pool_info = vk::DescriptorPoolCreateInfo {
            s_type: vk::StructureType::DESCRIPTOR_POOL_CREATE_INFO,
            p_next: ptr::null(),
            flags: vk::DescriptorPoolCreateFlags::empty(),
            max_sets: cfg::MAX_FRAMES_IN_FLIGHT,
            pool_size_count: pool_sizes.len() as u32,
            p_pool_sizes: pool_sizes.as_ptr(),
        };

        unsafe {
            device
                .create_descriptor_pool(&pool_info, None)
                .expect("failed to create descriptor pool")
        }
    }

    fn create_descriptor_sets(
        device: &ash::Device,
        pool: vk::DescriptorPool,
        layout: vk::DescriptorSetLayout,
        swapchain_img_size: usize,
        uniform_buffers: &[BufferData],
        texture_sampler: vk::Sampler,
        texture_img_view: vk::ImageView,
    ) -> Vec<vk::DescriptorSet> {
        let layouts: Vec<_> = (0..swapchain_img_size)
            .into_iter()
            .map(|_| layout)
            .collect();

        let alloc_info = vk::DescriptorSetAllocateInfo {
            s_type: vk::StructureType::DESCRIPTOR_SET_ALLOCATE_INFO,
            p_next: ptr::null(),
            descriptor_pool: pool,
            descriptor_set_count: cfg::MAX_FRAMES_IN_FLIGHT,
            p_set_layouts: layouts.as_ptr(),
        };

        let descriptors = unsafe {
            device
                .allocate_descriptor_sets(&alloc_info)
                .expect("failed to create descriptor sets")
        };

        for (i, _des) in descriptors.iter().enumerate() {
            let buffer_info = vk::DescriptorBufferInfo {
                buffer: uniform_buffers[i].buffer,
                offset: 0,
                range: std::mem::size_of::<UniformBufferObject>() as u64,
            };

            let img_info = vk::DescriptorImageInfo {
                sampler: texture_sampler,
                image_view: texture_img_view,
                image_layout: vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
            };

            let descriptor_writes = [
                // uniform buffer
                vk::WriteDescriptorSet {
                    s_type: vk::StructureType::WRITE_DESCRIPTOR_SET,
                    p_next: ptr::null(),
                    dst_set: descriptors[i],
                    dst_binding: 0,
                    dst_array_element: 0,
                    descriptor_count: 1,
                    descriptor_type: vk::DescriptorType::UNIFORM_BUFFER,
                    p_image_info: ptr::null(),
                    p_buffer_info: &buffer_info,
                    p_texel_buffer_view: ptr::null(),
                },
                // sampler
                vk::WriteDescriptorSet {
                    s_type: vk::StructureType::WRITE_DESCRIPTOR_SET,
                    p_next: ptr::null(),
                    dst_set: descriptors[i],
                    dst_binding: 1,
                    dst_array_element: 0,
                    descriptor_count: 1,
                    descriptor_type: vk::DescriptorType::COMBINED_IMAGE_SAMPLER,
                    p_image_info: &img_info,
                    p_buffer_info: ptr::null(),
                    p_texel_buffer_view: ptr::null(),
                },
            ];

            unsafe {
                device.update_descriptor_sets(&descriptor_writes, &[]);
            }
        }

        descriptors
    }

    fn create_texture_img(
        instance: &ash::Instance,
        device: &ash::Device,
        phys_device: vk::PhysicalDevice,
        cmd_pool: vk::CommandPool,
        queue: vk::Queue,
        img_path: &Path,
    ) -> ImgData {
        let img_obj = image::open(img_path).unwrap();
        img_obj.flipv();

        let (img_width, img_height) = (img_obj.width(), img_obj.height());
        let img_size =
            (std::mem::size_of::<u8>() as u32 * img_width * img_height * 4) as vk::DeviceSize;

        let img_data = match &img_obj {
            image::DynamicImage::ImageLuma8(_) | image::DynamicImage::ImageRgb8(_) => {
                img_obj.to_rgba8().into_raw()
            }
            image::DynamicImage::ImageLumaA8(_) | image::DynamicImage::ImageRgba8(_) => {
                img_obj.into_bytes()
            }
            _ => todo!(),
        };

        if img_size == 0 {
            panic!("Failed to load texture img!");
        }

        let staging_buff = BufferData::new(
            instance,
            device,
            phys_device,
            img_size,
            vk::BufferUsageFlags::TRANSFER_SRC,
            vk::MemoryPropertyFlags::HOST_VISIBLE | vk::MemoryPropertyFlags::HOST_COHERENT,
            vk::SharingMode::EXCLUSIVE,
            None,
        );

        unsafe {
            let data_ptr = device
                .map_memory(staging_buff.mem, 0, img_size, vk::MemoryMapFlags::empty())
                .expect("failed to map texture memory") as *mut u8;
            data_ptr.copy_from_nonoverlapping(img_data.as_ptr(), img_data.len());
            device.unmap_memory(staging_buff.mem);
        }

        let texture_img = img_utils::create_img(
            instance,
            device,
            phys_device,
            img_width,
            img_height,
            None,
            vk::SampleCountFlags::TYPE_1,
            vk::Format::R8G8B8A8_SRGB,
            vk::ImageTiling::OPTIMAL,
            vk::ImageUsageFlags::TRANSFER_SRC
                | vk::ImageUsageFlags::TRANSFER_DST
                | vk::ImageUsageFlags::SAMPLED,
            vk::MemoryPropertyFlags::DEVICE_LOCAL,
            vk::ImageAspectFlags::COLOR,
        );

        img_utils::transition_img_layout(
            device,
            cmd_pool,
            queue,
            texture_img.img,
            texture_img.mip_levels,
            vk::Format::R8G8B8A8_SRGB,
            vk::ImageLayout::UNDEFINED, // we don't care about the content, before performing the copy operation
            vk::ImageLayout::TRANSFER_DST_OPTIMAL,
            vk::ImageAspectFlags::COLOR,
        );

        img_utils::copy_buffer_to_img(
            device,
            cmd_pool,
            queue,
            staging_buff.buffer,
            texture_img.img,
            img_width,
            img_height,
        );

        // prepare img for being sampled
        // moved to generate-mipmaps
        // img_utils::transition_img_layout(
        //     device,
        //     cmd_pool,
        //     queue,
        //     texture_img.img,
        //     texture_img.mip_levels,
        //     vk::Format::R8G8B8A8_SRGB,
        //     vk::ImageLayout::TRANSFER_DST_OPTIMAL,
        //     vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
        //     vk::ImageAspectFlags::COLOR,
        // );

        img_utils::generate_mipmaps(
            instance,
            device,
            phys_device,
            queue,
            cmd_pool,
            texture_img.img,
            vk::Format::R8G8B8A8_SRGB,
            img_width,
            img_height,
            texture_img.mip_levels,
        );

        unsafe {
            device.destroy_buffer(staging_buff.buffer, None);
            device.free_memory(staging_buff.mem, None);
        }

        texture_img
    }

    #[allow(clippy::too_many_arguments)]
    fn create_depth_resources(
        instance: &ash::Instance,
        device: &ash::Device,
        phys_device: vk::PhysicalDevice,
        cmd_pool: vk::CommandPool,
        queue: vk::Queue,
        width: u32,
        height: u32,
        msaa_samples: vk::SampleCountFlags,
    ) -> ImgData {
        let depth_format = Self::find_depth_format(instance, phys_device);

        let img_data = img_utils::create_img(
            instance,
            device,
            phys_device,
            width,
            height,
            Some(1),
            msaa_samples,
            depth_format,
            vk::ImageTiling::OPTIMAL,
            vk::ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT,
            vk::MemoryPropertyFlags::DEVICE_LOCAL,
            vk::ImageAspectFlags::DEPTH,
        );

        // not required to explicityl change the img-layout
        img_utils::transition_img_layout(
            device,
            cmd_pool,
            queue,
            img_data.img,
            1,
            depth_format,
            vk::ImageLayout::UNDEFINED,
            vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
            vk::ImageAspectFlags::DEPTH,
        );

        img_data
    }

    fn find_depth_format(instance: &ash::Instance, phys_device: vk::PhysicalDevice) -> vk::Format {
        device_analysis::find_supported_foramt(
            instance,
            phys_device,
            &[
                vk::Format::D32_SFLOAT,
                vk::Format::D32_SFLOAT_S8_UINT,
                vk::Format::D24_UNORM_S8_UINT,
            ],
            vk::ImageTiling::OPTIMAL,
            vk::FormatFeatureFlags::DEPTH_STENCIL_ATTACHMENT,
        )
    }

    fn _has_stencil_component(format: vk::Format) -> bool {
        format == vk::Format::D32_SFLOAT_S8_UINT || format == vk::Format::D24_UNORM_S8_UINT
    }

    fn create_color_resources(
        instance: &ash::Instance,
        device: &ash::Device,
        phys_device: vk::PhysicalDevice,
        format: vk::Format,
        extent: vk::Extent2D,
        msaa_samples: vk::SampleCountFlags,
    ) -> ImgData {
        img_utils::create_img(
            instance,
            device,
            phys_device,
            extent.width,
            extent.height,
            Some(1),
            msaa_samples,
            format,
            vk::ImageTiling::OPTIMAL,
            vk::ImageUsageFlags::TRANSIENT_ATTACHMENT | vk::ImageUsageFlags::COLOR_ATTACHMENT,
            vk::MemoryPropertyFlags::DEVICE_LOCAL,
            vk::ImageAspectFlags::COLOR,
        )
    }
}

impl VulkanApp {
    fn record_command_buffer(
        &self,
        command_buffer: vk::CommandBuffer,
        image_idx: u32,
        indices: &[u32],
    ) {
        let begin_info = vk::CommandBufferBeginInfo {
            s_type: vk::StructureType::COMMAND_BUFFER_BEGIN_INFO,
            p_next: ptr::null(),
            flags: Default::default(),
            p_inheritance_info: ptr::null(),
        };

        unsafe {
            self.device
                .begin_command_buffer(command_buffer, &begin_info)
                .expect("failed to begin command-buffer");
        }

        // one clear-color per attachment with load-op-clear - order should be identical
        let clear_values = [
            vk::ClearValue {
                color: vk::ClearColorValue {
                    float32: [0.0, 0.0, 0.0, 1.0],
                },
            },
            vk::ClearValue {
                // range of depths 0.0 - 1.0 in Vulkan
                // 1.0 = far-view-plane - 0.0 = new-view-plane
                // initial value = furhest away value = 1.0
                depth_stencil: vk::ClearDepthStencilValue {
                    depth: 1.0,
                    stencil: 0,
                },
            },
        ];

        let render_pass_info = vk::RenderPassBeginInfo {
            s_type: vk::StructureType::RENDER_PASS_BEGIN_INFO,
            p_next: ptr::null(),
            render_pass: self.render_pass,
            framebuffer: self.framebuffers[image_idx as usize],
            render_area: vk::Rect2D {
                // should match size of attachments, for bp
                offset: vk::Offset2D { x: 0, y: 0 },
                extent: self.swapchain_data.extent,
            },
            clear_value_count: clear_values.len() as u32,
            p_clear_values: clear_values.as_ptr(),
        };

        // record commands
        // ---------------
        unsafe {
            self.device.cmd_begin_render_pass(
                command_buffer,
                &render_pass_info,
                vk::SubpassContents::INLINE,
            );
            self.device.cmd_bind_pipeline(
                command_buffer,
                vk::PipelineBindPoint::GRAPHICS,
                self.pipeline_data.graphics_pipeline,
            );

            self.device.cmd_bind_vertex_buffers(
                command_buffer,
                0,
                &[self.vertex_data.buffer],
                &[0],
            );
            self.device.cmd_bind_index_buffer(
                command_buffer,
                self.index_data.buffer,
                0,
                vk::IndexType::UINT32,
            );

            self.device.cmd_bind_descriptor_sets(
                command_buffer,
                vk::PipelineBindPoint::GRAPHICS,
                self.pipeline_data.pipeline_layout,
                0,
                &[self.descriptor_sets[self.curr_frame as usize]],
                &[],
            );

            // self.device.cmd_draw(command_buffer, cfg::VERTICES.len() as u32, 1, 0, 0);
            self.device
                .cmd_draw_indexed(command_buffer, indices.len() as u32, 1, 0, 0, 0);

            self.device.cmd_end_render_pass(command_buffer);
        }

        unsafe {
            self.device
                .end_command_buffer(command_buffer)
                .expect("failed to record command-buffer");
        }
    }
}

impl Drop for VulkanApp {
    fn drop(&mut self) {
        dbg!("VulkanApp - drop: dropping app...");

        unsafe {
            // color
            // -----
            self.device.destroy_image_view(self.color_data.view, None);
            self.device.destroy_image(self.color_data.img, None);
            self.device.free_memory(self.color_data.mem, None);

            // depth
            // -----
            self.device.destroy_image_view(self.depth_data.view, None);
            self.device.destroy_image(self.depth_data.img, None);
            self.device.free_memory(self.depth_data.mem, None);

            // textures
            // --------
            self.device.destroy_sampler(self.texture_sampler, None);
            self.device.destroy_image_view(self.texture_data.view, None);
            self.device.destroy_image(self.texture_data.img, None);
            self.device.free_memory(self.texture_data.mem, None);

            // buffers
            // -------
            self.device.destroy_buffer(self.vertex_data.buffer, None);
            // memory that is bound to a buffer obj, may be freed once the buffer is no longer used
            self.device.free_memory(self.vertex_data.mem, None);

            self.device.destroy_buffer(self.index_data.buffer, None);
            self.device.free_memory(self.index_data.mem, None);

            // sync-data
            // ---------
            self.sync_data.iter().for_each(|d| {
                self.device.destroy_semaphore(d.img_available_sem, None);
                self.device.destroy_semaphore(d.render_finished_sem, None);
                self.device.destroy_fence(d.in_flight_fence, None);
            });

            // command-pools
            // -------------
            // will destroy all associated command-buffers
            self.device
                .destroy_command_pool(self.graphics_cmd_pool, None);
            self.device
                .destroy_command_pool(self.transfer_cmd_pool, None);

            // framebuffers
            // ------------
            self.framebuffers.iter().for_each(|b| {
                self.device.destroy_framebuffer(*b, None);
            });

            // uniform-buffers
            // ---------------
            self.uniform_data.iter().for_each(|b| {
                self.device.destroy_buffer(b.buffer, None);
                self.device.free_memory(b.mem, None);
            });

            // descriptors
            // -----------
            // will automatically clean up any associated descriptor-set
            self.device
                .destroy_descriptor_pool(self.descriptor_pool, None);
            self.device
                .destroy_descriptor_set_layout(self.descriptor_set_layout, None);

            // pipeline
            // --------
            self.device
                .destroy_pipeline(self.pipeline_data.graphics_pipeline, None);
            self.device
                .destroy_pipeline_layout(self.pipeline_data.pipeline_layout, None);
            self.device.destroy_render_pass(self.render_pass, None);

            // swap-chain
            // ----------
            self.swapchain_data.img_views.iter().for_each(|&v| {
                self.device.destroy_image_view(v, None);
            });

            // cleans-up all swapchain-images
            self.swapchain_data
                .swapchain_loader
                .destroy_swapchain(self.swapchain_data.swapchain, None);

            // queues
            // ------
            // cleans-up device-queues
            self.device.destroy_device(None);

            // surface
            // -------
            self.surface_data
                .surface_loader
                .destroy_surface(self.surface_data.surface, None);

            // validation-layers
            // -----------------
            if cfg::ENABLE_VALIDATION_LAYERS {
                self.debug_data
                    .debug_utils_loader
                    .destroy_debug_utils_messenger(self.debug_data.debug_messenger, None);
            }

            // instance
            // --------
            self.instance.destroy_instance(None);
        }
    }
}

impl VulkanApp {
    pub fn main_loop(mut self, event_loop: EventLoop<()>) {
        let mut fps = crate::utils::fps_limiter::FPSLimiter::new();

        event_loop.run(move |event, _, control_flow| match event {
            Event::WindowEvent { event, .. } => match event {
                WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
                WindowEvent::KeyboardInput {
                    input:
                        KeyboardInput {
                            virtual_keycode: Some(VirtualKeyCode::Escape),
                            state: ElementState::Pressed,
                            ..
                        },
                    ..
                } => {
                    dbg!("> escape pressed");
                    *control_flow = ControlFlow::Exit;
                }
                _ => (),
            },
            Event::MainEventsCleared => {
                self.window.request_redraw();
            }
            Event::RedrawRequested(_) => {
                let delta_time = fps.delta_time();
                self.draw_frame(delta_time);

                fps.tick_frame();
            }
            Event::LoopDestroyed => unsafe {
                self.device.device_wait_idle().unwrap();
            },
            _ => {}
        });
    }

    fn draw_frame(&mut self, delta_time: f32) {
        let curr_frame = self.curr_frame as usize;

        self.update_uniform_buffer(curr_frame, delta_time);

        unsafe {
            self.device
                .wait_for_fences(
                    &[self.sync_data[curr_frame].in_flight_fence],
                    true,
                    u64::MAX,
                )
                .unwrap();
            self.device
                .reset_fences(&[self.sync_data[curr_frame].in_flight_fence])
                .unwrap();
        }

        let (img_idx, _) = unsafe {
            self.swapchain_data
                .swapchain_loader
                .acquire_next_image(
                    self.swapchain_data.swapchain,
                    u64::MAX,
                    self.sync_data[curr_frame].img_available_sem,
                    vk::Fence::null(),
                )
                .unwrap()
        };

        unsafe {
            self.device
                .reset_command_buffer(
                    self.graphics_cmd_buffers[curr_frame],
                    vk::CommandBufferResetFlags::empty(),
                )
                .unwrap();
            self.record_command_buffer(
                self.graphics_cmd_buffers[curr_frame],
                img_idx,
                &self.model.indices,
            );
        }

        let wait_semaphores = [self.sync_data[curr_frame].img_available_sem];
        let wait_stages = [vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT];
        let signal_semaphores = [self.sync_data[curr_frame].render_finished_sem];

        let submit_info = [vk::SubmitInfo {
            s_type: vk::StructureType::SUBMIT_INFO,
            p_next: ptr::null(),
            wait_semaphore_count: wait_semaphores.len() as u32,
            p_wait_semaphores: wait_semaphores.as_ptr(),
            p_wait_dst_stage_mask: wait_stages.as_ptr(),
            command_buffer_count: 1,
            p_command_buffers: &self.graphics_cmd_buffers[curr_frame],
            signal_semaphore_count: signal_semaphores.len() as u32,
            p_signal_semaphores: signal_semaphores.as_ptr(),
        }];

        unsafe {
            self.device
                .queue_submit(
                    self.queue_data.graphics_queue,
                    &submit_info,
                    self.sync_data[curr_frame].in_flight_fence,
                )
                .expect("failed to submit queue");
        }

        let swapchains = [self.swapchain_data.swapchain];

        let present_info = vk::PresentInfoKHR {
            s_type: vk::StructureType::PRESENT_INFO_KHR,
            p_next: ptr::null(),
            wait_semaphore_count: signal_semaphores.len() as u32,
            p_wait_semaphores: signal_semaphores.as_ptr(),
            swapchain_count: swapchains.len() as u32,
            p_swapchains: swapchains.as_ptr(),
            p_image_indices: [img_idx].as_ptr(),
            p_results: ptr::null_mut(),
        };

        unsafe {
            let _ = self
                .swapchain_data
                .swapchain_loader
                .queue_present(self.queue_data.present_queue, &present_info)
                .expect("failed to present queue");
        }

        self.curr_frame = (self.curr_frame + 1) % cfg::MAX_FRAMES_IN_FLIGHT;
    }

    fn update_uniform_buffer(&mut self, curr_img: usize, delta_time: f32) {
        self.uniform_transform.model =
            Matrix4::from_angle_z(cgmath::Deg(90.0) * (delta_time / 2.0))
                * self.uniform_transform.model;

        let ubos = [self.uniform_transform.clone()];
        let buff_size = (std::mem::size_of::<UniformBufferObject>() * ubos.len()) as u64;

        let curr_data = &self.uniform_data[curr_img];

        unsafe {
            let data_ptr =
                self.device
                    .map_memory(curr_data.mem, 0, buff_size, vk::MemoryMapFlags::empty())
                    .expect("failed to map memory") as *mut UniformBufferObject;

            data_ptr.copy_from_nonoverlapping(ubos.as_ptr(), ubos.len());

            self.device.unmap_memory(curr_data.mem);
        }
    }
}
